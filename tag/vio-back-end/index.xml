<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>VIO Back-End | NUSUAV</title>
    <link>/tag/vio-back-end/</link>
      <atom:link href="/tag/vio-back-end/index.xml" rel="self" type="application/rss+xml" />
    <description>VIO Back-End</description>
    <generator>Source Themes Academic (https://sourcethemes.com/academic/)</generator><language>en-us</language><copyright>© 2020 NUSUAV</copyright><lastBuildDate>Thu, 30 Jul 2020 00:00:00 +0000</lastBuildDate>
    <image>
      <url>/images/icon_hu0b7a4cb9992c9ac0e91bd28ffd38dd00_9727_512x512_fill_lanczos_center_2.png</url>
      <title>VIO Back-End</title>
      <link>/tag/vio-back-end/</link>
    </image>
    
    <item>
      <title>VIO (Backend, Graph Optimization Based)</title>
      <link>/2_vio/vio-back-end/</link>
      <pubDate>Thu, 30 Jul 2020 00:00:00 +0000</pubDate>
      <guid>/2_vio/vio-back-end/</guid>
      <description>&lt;h1 id=&#34;basalt-backend-walkthrough&#34;&gt;Basalt Backend Walkthrough&lt;/h1&gt;
&lt;h2 id=&#34;overview--class-names&#34;&gt;Overview &amp;amp; Class Names&lt;/h2&gt;
&lt;p&gt;The backend of the VIO framework is contained with the base class &lt;code&gt;VioEstimatorBase&lt;/code&gt; (defined in &lt;code&gt;vio_estimator.h&lt;/code&gt;) which is the parent class of two type of the estimator possible:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;code&gt;KeypointVioEstimator&lt;/code&gt;(defined in &lt;code&gt;keypoint_vio.h&lt;/code&gt;), and&lt;/li&gt;
&lt;li&gt;&lt;code&gt;KeypointVoEstimator&lt;/code&gt;(defined in &lt;code&gt;keypoint_vo.h&lt;/code&gt;)&lt;/li&gt;
&lt;/ul&gt;
&lt;pre&gt;&lt;code class=&#34;language-cpp&#34;&gt;// how the factory class initialise to different type of estimator depends on use_imu variable
VioEstimatorBase::Ptr VioEstimatorFactory::getVioEstimator(
    const VioConfig&amp;amp; config, const Calibration&amp;lt;double&amp;gt;&amp;amp; cam,
    const Eigen::Vector3d&amp;amp; g, bool use_imu) {
  VioEstimatorBase::Ptr res;

  if (use_imu) {
    res.reset(new KeypointVioEstimator(g, cam, config));
  } else {
    res.reset(new KeypointVoEstimator(cam, config));
  }

  return res;
}
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;For the purpose of this walkthrough honly &lt;code&gt;KeypointVioEstimator&lt;/code&gt; is conerned. We are interested in understanding how optical flow observations (keypoints) are used to form keyframes, and 3D landmarks.&lt;/p&gt;
&lt;h2 id=&#34;io-of-base-class-of-vioestimator&#34;&gt;I/O of Base Class of VioEstimator&lt;/h2&gt;
&lt;p&gt;The &lt;strong&gt;inputs&lt;/strong&gt; are optical results (observations of keypoints) and imu inputs:&lt;/p&gt;
&lt;ol&gt;
&lt;li&gt;A TBB queue, &lt;code&gt;vision_data_queue&lt;/code&gt; of type &lt;code&gt;OpticalFlowResult::Ptr&lt;/code&gt;, where each optical flow result contains
&lt;ol&gt;
&lt;li&gt;the 64-bit timestamp,&lt;/li&gt;
&lt;li&gt;a vector named &lt;code&gt;observations&lt;/code&gt;, containing a map between the keypoint ID &lt;code&gt;KeypointId&lt;/code&gt;  and its 2D transformations &lt;code&gt;Eigen::AffineCompact2f&lt;/code&gt; (2D transformation literally represent the keypoint 2D location in the observing camera frame, aka. the uv coordinates)&lt;/li&gt;
&lt;li&gt;and a pointer to the original image data (&lt;code&gt;std::vector&amp;lt;ImageData&amp;gt;&lt;/code&gt;), for the current frame&lt;/li&gt;
&lt;/ol&gt;
&lt;/li&gt;
&lt;li&gt;A TBB queue, &lt;code&gt;imu_data_queue&lt;/code&gt; of type &lt;code&gt;ImuData::Ptr&lt;/code&gt;, storing the timestamp, and the accelerometer and gyroscope information&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;The &lt;strong&gt;outputs&lt;/strong&gt; are output states, ⚠️ marginalised data?, and vio visualisation data:&lt;/p&gt;
&lt;ol&gt;
&lt;li&gt;A pointer to TBB queue &lt;code&gt;out_state_queue&lt;/code&gt; of type &lt;code&gt;PoseVelBiasState::Ptr&lt;/code&gt; containing all the states which are: &lt;code&gt;t_ns&lt;/code&gt; timestamp of the state in nanoseconds; &lt;code&gt;T_w_i&lt;/code&gt; pose of the state; &lt;code&gt;vel_w_i&lt;/code&gt; linear velocity of the state; &lt;code&gt;bias_gyro&lt;/code&gt; gyroscope bias; &lt;code&gt;bias_accel  &lt;/code&gt;accelerometer bias.&lt;/li&gt;
&lt;li&gt;A pointer to TBB queue &lt;code&gt;out_marg_queue&lt;/code&gt; of type &lt;code&gt;MargData::Ptr&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;A pointer to TBB queue&lt;code&gt;out_vis_queue&lt;/code&gt; of type &lt;code&gt;VioVisualizationData::Ptr&lt;/code&gt;&lt;/li&gt;
&lt;/ol&gt;
&lt;h2 id=&#34;initialisation&#34;&gt;Initialisation&lt;/h2&gt;
&lt;p&gt;With the incoming flow of visual optical flow observations and IMU data, the backend has a way to initialise itself with a proper initial pose (and velocity) and bias estimate. We can choose to initialise either just the biases and attitude (quaternion), or with the transformation and velocity as well.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Key things to note:&lt;/strong&gt;&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;The current implementation in Basalt only does bias and quaternion initialisation (all others set): bias is &lt;u&gt;set them to zero&lt;/u&gt;, and attitude is done by two-vector calculation from single data point of imu -&amp;gt; &lt;strong&gt;TODO [Improvement]&lt;/strong&gt;&lt;/li&gt;
&lt;li&gt;The initial IMU-world transformation &lt;code&gt;T_w_i_init&lt;/code&gt; has a very special way to initialise, which is yaw ignorant:
&lt;ul&gt;
&lt;li&gt;First the accelerometer raw reading is used (one data point), it is assumed to be the gravity vector&lt;/li&gt;
&lt;li&gt;The gravity vector is rotated to be aligned with the positive Z axis, this rotation action is recorded as a quaternion. This quaternion is essentially performing basis changing from body frame to global frame.&lt;/li&gt;
&lt;li&gt;That is to say, the transformation is only defined by the plane that the gravity vector and the +ve z-axis make as well as the angle itself. This does not take account into the correct yaw, a.k.a the initial heading is not consistent, which is a function of how the IMU is mounted and its initial orientation.&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;NOTE&lt;/strong&gt;: Therefore, with a fixed mounting between the IMU and camera, and with a fixed up direction of the whole rig (e.g. the drone), we can pre-calculate the rotation matrix to make the initialised $T$ transformation matrix to face our desired coordinate convention (e.g. NWU).&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;The pre-integration buffer &lt;code&gt;imu_meas&lt;/code&gt; is also initialised here, taken account of the bias (which is always set to zeros in the implementation)&lt;/li&gt;
&lt;li&gt;The bundle adjustment states &lt;code&gt;frame_states&lt;/code&gt; of type &lt;code&gt; Eigen::aligned_map&amp;lt;int64_t, PoseVelBiasStateWithLin&amp;gt;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;code&gt;marg_order&lt;/code&gt;??&lt;/li&gt;
&lt;li&gt;The processing thread is created within the &lt;code&gt;initialize()&lt;/code&gt; function, which is basically a &lt;strong&gt;while&lt;/strong&gt; loop, by the means of lamda function and &lt;code&gt;std::thread&lt;/code&gt;.&lt;/li&gt;
&lt;/ul&gt;
&lt;h2 id=&#34;the-processing-thread-within-keypointvioestimator&#34;&gt;The Processing Thread (within &lt;code&gt;KeypointVioEstimator&lt;/code&gt;)&lt;/h2&gt;
&lt;p&gt;&lt;strong&gt;Configs&lt;/strong&gt;:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;&lt;code&gt;vio_enforce_realtime&lt;/code&gt; is used to set when the backend cannot catch up with optical flow results. It will throw away all previous results, except the latest one in the queue.&lt;/p&gt;
&lt;pre&gt;&lt;code class=&#34;language-cpp&#34;&gt;if (config.vio_enforce_realtime) {
        // drop current frame if another frame is already in the queue.
        while (vision_data_queue.try_pop(curr_frame)) {skipped_image++;}
        if(skipped_image)
          std::cerr&amp;lt;&amp;lt; &amp;quot;[Warning] skipped opt flow size: &amp;quot;&amp;lt;&amp;lt;skipped_image&amp;lt;&amp;lt;std::endl;
      }
      if (!curr_frame.get()) {
        break;
      }
&lt;/code&gt;&lt;/pre&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;code&gt;vio_min_triangulation_dist&lt;/code&gt; is used to determine whether the two camera frames are suitable for triangulation in generating the landmarks, during a keyframe initialisation&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;code&gt;vio_new_kf_keypoints_thresh&lt;/code&gt; is the threshold for the ratio between tracked landmarks and total tracked keypoints (e.g. raito of 0.7)&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;&lt;code&gt;vio_min_frames_after_kf&lt;/code&gt;&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;strong&gt;Calibs&lt;/strong&gt;:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;&lt;code&gt;cam_time_offset_ns&lt;/code&gt; offset of the camera in time, should be normally 0&lt;/p&gt;
&lt;pre&gt;&lt;code class=&#34;language-cpp&#34;&gt;// Correct camera time offset
      curr_frame-&amp;gt;t_ns += calib.cam_time_offset_ns;
&lt;/code&gt;&lt;/pre&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;After initialisation, the actual processing will only start with two concecutive frames (when both &lt;code&gt;prev_frame&lt;/code&gt; and &lt;code&gt;curr_frame&lt;/code&gt; are defined):&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Pre-integration is performed, between the two frames, using the latest bias estimation. Blocking may happen, as it reads IMU buffer all the way until one pass the current frame&amp;rsquo;s timestamp (&lt;code&gt;while (data-&amp;gt;t_ns &amp;lt;= curr_frame-&amp;gt;t_ns)&lt;/code&gt;)
&lt;ul&gt;
&lt;li&gt;Note: the integration will ensure the upper integral timestamp is at least the current frame timestamp (it will make fake IMU measurement by shifting the last IMU readings, when needed)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;At the same time, bias correction &lt;code&gt;getCalibrated()&lt;/code&gt; are also done for both gyro and accelerometer&lt;/li&gt;
&lt;li&gt;The bulk of the calculation is doen within the &lt;code&gt;measure(curr_frame, meas)&lt;/code&gt; function, which is discussed next&lt;/li&gt;
&lt;/ul&gt;
&lt;h2 id=&#34;the-measure-routine&#34;&gt;The &lt;code&gt;Measure()&lt;/code&gt; Routine&lt;/h2&gt;
&lt;p&gt;The &lt;code&gt;measure()&lt;/code&gt; routine takes in the current frame optical flow observations, as well as the IMU pre-integration results.&lt;/p&gt;
&lt;h3 id=&#34;imu-measurements-processing&#34;&gt;IMU measurements Processing&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;&lt;code&gt;frame_states&lt;/code&gt; is updated with a entry key &lt;code&gt;last_state_t_ns&lt;/code&gt; (current frame timestamp), to be stored with the IMU predicted state&lt;/li&gt;
&lt;li&gt;&lt;code&gt;imu_meas&lt;/code&gt; is also updated by indexed by the previous frame&amp;rsquo;s timestamp&lt;/li&gt;
&lt;li&gt;This whole step might be skipped if IMU measurement is not passed in&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id=&#34;optical-flow-results-processing&#34;&gt;Optical Flow Results Processing&lt;/h3&gt;
&lt;p&gt;Pre-processing:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;&lt;code&gt;prev_opt_flow_res&lt;/code&gt; stores, with key in timestamp, the optical flow measurement struct pointer, up to the current time frame&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;For each camera frame, iterate through all observations. If that observed keypoint is already a landmark, add the observation to the landmark database; if not put it to the unconnected observation &lt;strong&gt;set&lt;/strong&gt;.&lt;/p&gt;
&lt;pre&gt;&lt;code class=&#34;language-cpp&#34;&gt;// skeleton
for (size_t i = 0; i &amp;lt; opt_flow_meas-&amp;gt;observations.size(); i++)
    for (const auto&amp;amp; kv_obs : opt_flow_meas-&amp;gt;observations[i])
        if (lmdb.landmarkExists(kpt_id)){
            num_points_connected[tcid_host.frame_id]++;
        }else{
            unconnected_obs0.emplace(kpt_id);
        }
&lt;/code&gt;&lt;/pre&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Key-framing:&lt;/p&gt;
&lt;h1 id=&#34;to-be-continued&#34;&gt;To be continued&amp;hellip;&lt;/h1&gt;
</description>
    </item>
    
  </channel>
</rss>
