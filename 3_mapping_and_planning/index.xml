<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>3_mapping_and_plannings | NUSUAV</title>
    <link>/3_mapping_and_planning/</link>
      <atom:link href="/3_mapping_and_planning/index.xml" rel="self" type="application/rss+xml" />
    <description>3_mapping_and_plannings</description>
    <generator>Source Themes Academic (https://sourcethemes.com/academic/)</generator><language>en-us</language><copyright>© 2020 NUSUAV</copyright><lastBuildDate>Thu, 30 Jul 2020 00:00:00 +0000</lastBuildDate>
    <image>
      <url>/images/icon_hu0b7a4cb9992c9ac0e91bd28ffd38dd00_9727_512x512_fill_lanczos_center_2.png</url>
      <title>3_mapping_and_plannings</title>
      <link>/3_mapping_and_planning/</link>
    </image>
    
    <item>
      <title>Mapping</title>
      <link>/3_mapping_and_planning/mapping/</link>
      <pubDate>Thu, 30 Jul 2020 00:00:00 +0000</pubDate>
      <guid>/3_mapping_and_planning/mapping/</guid>
      <description>&lt;h1 id=&#34;overview&#34;&gt;Overview&lt;/h1&gt;
&lt;p&gt;A 3D reconstruction system for MAVs using GPU is implemented in our work, which gives strong support of data to the navigation and decision making in motion planning. Maintaining and updating a 3D map and planning with the global map are both computational costly. Moreover, the global map can be erroneous with noise and estimation drift. Therefore, we maintain a local map only that provides obstacles information around current robot location.&lt;/p&gt;
&lt;p&gt;An environmental occupancy grid map is generated by updating the stereo camera and 2D Rplidar data with voxel projection method. In our implementation, the depth image is transferred to the point cloud and applied with several filters, such as voxel grid downsampling and range pass-through filters to clean the reduce data size. As the limited field of view of the camera, the 2D Rplidar may also be utilized for more accurate and robust mapping. And then euclidean distance filed (EDF) is constructed through Euclidean Distance Transform (EDT).&lt;/p&gt;
&lt;p&gt;Our occupancy grid  map is built with voxel projection method. Different from the traditional ray casting method, which reprojects the pixel from depth image to correspondent depth voxel, the voxel projection method divides voxel space to scanlines and projects voxels in space to the image plane. The latter approach is faster. More information please refer to our internal report.&lt;/p&gt;
&lt;body&gt;&lt;img src=&#34;map_sim.png&#34;  width=&#34;600&#34; height=&#34;&#34; &gt;&lt;p style=&#34;text-align:center;&#34;&gt;Map in simulation testing&lt;/p&gt;&lt;/body&gt;
&lt;body&gt;&lt;img src=&#34;map_corridor.png&#34;  width=&#34;600&#34; height=&#34;&#34; &gt;&lt;p style=&#34;text-align:center;&#34;&gt;Map in real flight testing&lt;/p&gt;&lt;/body&gt;
&lt;/br&gt;
&lt;h1 id=&#34;usage&#34;&gt;Usage&lt;/h1&gt;
&lt;h2 id=&#34;installation&#34;&gt;Installation&lt;/h2&gt;
&lt;p&gt;Refer to System Overview page. The edt repository is for stereo depth mapping.&lt;/p&gt;
&lt;h1 id=&#34;nodes&#34;&gt;Nodes&lt;/h1&gt;
&lt;p&gt;Put the following section in your launch file:&lt;/p&gt;
&lt;pre&gt;&lt;code&gt;    &amp;lt;include file=&amp;quot;$(find edt)/launch/edt_ddrone.launch&amp;quot; &amp;gt;
	    &amp;lt;arg name=&amp;quot;system_id&amp;quot; value=&amp;quot;$(arg system_id)&amp;quot;/&amp;gt;
    &amp;lt;/include&amp;gt;
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;Remember to set the system_id and topic remap in the launch file correctly.&lt;/p&gt;
&lt;h2 id=&#34;edt_node&#34;&gt;edt_node&lt;/h2&gt;
&lt;h3 id=&#34;subscribed-topics&#34;&gt;Subscribed topics&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;/revised_sensor/image (sensor_msgs/Image)&lt;/p&gt;
&lt;p&gt;Depth map from stereo camera.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/zed/confidence/confidence_map (sensor_msgs/Image)&lt;/p&gt;
&lt;p&gt;Confidence map of the depth.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/mavros/position/local (geometry_msgs/PoseStamped)&lt;/p&gt;
&lt;p&gt;UAV current pose&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/scan (sensor_msgs::LaserScan)&lt;/p&gt;
&lt;p&gt;Laser info&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id=&#34;published-topics&#34;&gt;Published topics&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;cost_map (edt::CostMap)&lt;/p&gt;
&lt;p&gt;Cost map used for planning&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;points3 (pcl::PointCloud&lt;a href=&#34;pcl::PointXYZ&#34;&gt;pcl::PointXYZ&lt;/a&gt;)&lt;/p&gt;
&lt;p&gt;Point cloud&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
</description>
    </item>
    
    <item>
      <title>Motion Planning</title>
      <link>/3_mapping_and_planning/planning/</link>
      <pubDate>Thu, 30 Jul 2020 00:00:00 +0000</pubDate>
      <guid>/3_mapping_and_planning/planning/</guid>
      <description>&lt;h1 id=&#34;overview&#34;&gt;Overview&lt;/h1&gt;
&lt;p&gt;To concatenate with the state estimation and mapping modules, a 3D motion planning module is designed for real-time path and trajectory generation, where a collision-free and dynamically feasible reference trajectory is generated to guide vehicles maneuver safely towards its desired target.&lt;/p&gt;
&lt;p&gt;We focus on the local planning problem, which is solved by Motion primitives (MPs), spanning a local search tree to abstract the continuous state space. We sample on the vehicles’ boundary state constraints and generate the actual motion by solving a boundary value problem. The generated MPs are termed as the boundary state constrained primitives (BSCPs).&lt;/p&gt;
&lt;p&gt;we utilize the neural network (NN) to learn the BSCPs solutions offline and approximate them with NN during online optimization. A gradient-free method using the NN and the particle swarm optimization (PSO) is designed to construct a dynamically evolving single layer tree that gradually converges to the optimal local target in the planning. Complex and long-range motions can then be generated by combining the local target with a sampling based global planner. More information please refer to our internal report.&lt;/p&gt;
&lt;h1 id=&#34;usage&#34;&gt;Usage&lt;/h1&gt;
&lt;h2 id=&#34;installation&#34;&gt;Installation&lt;/h2&gt;
&lt;p&gt;Please refer to System Overview page. The us_ws repository is for planning.&lt;/p&gt;
&lt;h1 id=&#34;nodes&#34;&gt;Nodes&lt;/h1&gt;
&lt;p&gt;Put at lease the following section in your launch file in order to use planning.&lt;/p&gt;
&lt;pre&gt;&lt;code&gt;    ## mapping
    &amp;lt;include file=&amp;quot;$(find edt)/launch/edt_ddrone.launch&amp;quot; &amp;gt;
	    &amp;lt;arg name=&amp;quot;system_id&amp;quot; value=&amp;quot;$(arg system_id)&amp;quot;/&amp;gt;
    &amp;lt;/include&amp;gt;

    ## motion planning
    &amp;lt;include file=&amp;quot;$(find nndp_cpp)/launch/nndp_ddrone.launch&amp;quot;&amp;gt;
        &amp;lt;arg name=&amp;quot;system_id&amp;quot; value=&amp;quot;$(arg system_id)&amp;quot;/&amp;gt;
        &amp;lt;arg name=&amp;quot;ctrlpred_location&amp;quot; value=&amp;quot;$(arg ctrlpred_location)&amp;quot;/&amp;gt;
    &amp;lt;/include&amp;gt;

    ## reference generator
    &amp;lt;include file=&amp;quot;$(find rt_ref_gen)/launch/refgen_ddrone.launch&amp;quot;&amp;gt;
         &amp;lt;arg name=&amp;quot;ctrlpred_location&amp;quot; value=&amp;quot;$(arg ctrlpred_location)&amp;quot;/&amp;gt;
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;If you want to use referece generator only, then add the third block only from the above code in your launch file. Please use the refgen_ddrone.launch as an example to design your own node. As the requirement vairies from different projects.&lt;/p&gt;
&lt;h2 id=&#34;1-edt_node&#34;&gt;1. edt_node&lt;/h2&gt;
&lt;p&gt;Please check out the mapping page.&lt;/p&gt;
&lt;h2 id=&#34;2-nndp_cpp_node&#34;&gt;2. nndp_cpp_node&lt;/h2&gt;
&lt;h3 id=&#34;subscribed-topics&#34;&gt;Subscribed topics&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;/cost_map (edt::CostMap)&lt;/p&gt;
&lt;p&gt;cost map from edt_node&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/dummy/pos_cmd (geometry_msgs::Point)&lt;/p&gt;
&lt;p&gt;target waypoints&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/engage_cmd (std_msgs::Bool)&lt;/p&gt;
&lt;p&gt;init planning command&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/rt_ref_gen/current_state (ommon_msgs::state)&lt;/p&gt;
&lt;p&gt;reference feedback&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/mavros/position/local (geometry_msgs::PoseStamped)&lt;/p&gt;
&lt;p&gt;UAV current pose from mavros. Stop subscribing this once after taking off as the feedback will come from the generated reference directly.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id=&#34;published-topics&#34;&gt;Published topics&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;points2 (pcl::PointCloud&lt;a href=&#34;pcl::PointXYZ&#34;&gt;pcl::PointXYZ&lt;/a&gt;)&lt;/p&gt;
&lt;p&gt;publish 2d point clouds&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;points_sp (pcl::PointCloud&lt;a href=&#34;pcl::PointXYZ&#34;&gt;pcl::PointXYZ&lt;/a&gt;)&lt;/p&gt;
&lt;p&gt;publish stereo point clouds&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/nndp_cpp/tgt_show (geometry_msgs::PoseStamped)&lt;/p&gt;
&lt;p&gt;planned target&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id=&#34;important-parameters&#34;&gt;Important Parameters&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;/home_name (string)&lt;/p&gt;
&lt;p&gt;The path of the trained network parameters and the reference generator policy files.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;fly_height (float)&lt;/p&gt;
&lt;p&gt;Take off height.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;system_id (int)&lt;/p&gt;
&lt;p&gt;Drone&amp;rsquo;s id.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;h2 id=&#34;3-rt_ref_gen_kerrigan&#34;&gt;3. rt_ref_gen_kerrigan&lt;/h2&gt;
&lt;h3 id=&#34;subscribed-topics-1&#34;&gt;Subscribed topics&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;/task_manager/mpc_init_position_nwu (geometry_msgs::PoseStamped)&lt;/p&gt;
&lt;p&gt;Initial pose to initialize the reference generator. This is normally the taking off pose.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;/nndp_cpp/tgt (common_msgs::target)&lt;/p&gt;
&lt;p&gt;Target points to generate reference.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id=&#34;published-topics-1&#34;&gt;Published topics&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;rt_ref_gen/current_state (common_msgs::state)&lt;/p&gt;
&lt;p&gt;Generated reference&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id=&#34;important-parameters-1&#34;&gt;Important Parameters&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;/home_name (string)&lt;/p&gt;
&lt;p&gt;The path of the trained network parameters and the reference generator policy files.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;h1 id=&#34;mpc-policy-generator-and-neural-network-training&#34;&gt;MPC policy generator and neural network training&lt;/h1&gt;
&lt;p&gt;The 
&lt;a href=&#34;https://bitbucket.org/nusuav/nndp.git&#34; target=&#34;_blank&#34; rel=&#34;noopener&#34;&gt;repo&lt;/a&gt; is matlab code for MPC policy generator and neural network training.&lt;/p&gt;
&lt;h2 id=&#34;data-explain&#34;&gt;Data Explain&lt;/h2&gt;
&lt;p&gt;Here we only care about triple_integrator part for each modules&lt;/p&gt;
&lt;h3 id=&#34;dp_part-dynamic-programming-policy-generator&#34;&gt;Dp_part: dynamic programming policy generator&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;
&lt;p&gt;horizontal&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;pi_hor.mat(trippleIntegrator_hor.m ): policy generated from for horizontal control&lt;/li&gt;
&lt;li&gt;q_bins: position range from -x to x&lt;/li&gt;
&lt;li&gt;qdot_bins: velocity range from -y to y&lt;/li&gt;
&lt;li&gt;qdd_bins: acceleration range from -z to z&lt;/li&gt;
&lt;li&gt;PI: policy, what is the control input given certain state&lt;/li&gt;
&lt;li&gt;J:  optimal value function J&lt;/li&gt;
&lt;li&gt;dt: dynamics discrete time (0.05)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;vertical&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;pi_ver.mat(from trippleIntegrator_ver.m): policy generated  for height control.&lt;/li&gt;
&lt;li&gt;q_bins_ver&lt;/li&gt;
&lt;li&gt;qdot_bins_ver&lt;/li&gt;
&lt;li&gt;qdd_bins_ver&lt;/li&gt;
&lt;li&gt;PI_ver&lt;/li&gt;
&lt;li&gt;J_ver&lt;/li&gt;
&lt;li&gt;dt&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;dt.bin, q_bins.bin, qdot_bin.bin, qdd_bins.bin, PI.bin, J.bin are for trajectory generation cpp code.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id=&#34;learning_part-prepare-the-data-for-training-and-validation&#34;&gt;Learning_part: prepare the data for training and validation&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;training_data_hor.mat (from DataGenerator_hor.m): data prepared for horizontal training, including n states with 20s prediction and u_square_total.&lt;/li&gt;
&lt;/ul&gt;
&lt;pre&gt;&lt;code&gt;training_data_hor{n,1} = [p v a]&#39;: initail state 
training_data_hor{n,2} = S: planned 20s of predicted 40 points 
training_data_hor{n,3} = u_sq_total: sum of u2 * dt for the total 40 points 
&lt;/code&gt;&lt;/pre&gt;
&lt;ul&gt;
&lt;li&gt;training_data_ver_small.mat (from DataGenerator_ver.m): data prepared for vertical network training, including n states with 20s prediction and u_square_total, same as training_data_hor.&lt;/li&gt;
&lt;li&gt;INISTATE.mat: save the initial state data from training_data_hor{n,1}&lt;/li&gt;
&lt;li&gt;TARGET.mat: save S and u_sq_total data from training_data_hor{n,2} and  training_data_hor{n,3}&lt;/li&gt;
&lt;li&gt;NN_hor.mat: trained 4-layer network parameter from array_0 to array_7.&lt;/li&gt;
&lt;/ul&gt;
&lt;h2 id=&#34;policy-change&#34;&gt;Policy change&lt;/h2&gt;
&lt;p&gt;To change the policy in MPC for trajectory generation, please follow the following steps. Take horizontal position control policy as an example:&lt;/p&gt;
&lt;ol&gt;
&lt;li&gt;Open the file /nndp/DP_part/triple_integrator/trippleIntegrator_hor.m&lt;/li&gt;
&lt;li&gt;Find line 19-21, where you will see&lt;/li&gt;
&lt;/ol&gt;
&lt;pre&gt;&lt;code&gt;q_bins = [-40:2:-12 -10:1:-5 -4.75:0.25:4.75 5:1:10 12:2:40]; 
qdot_bins = [-6:1:-5 -4.75:0.25:4.75 5:1:6]; 
qdd_bins = [-8:1:-5 -4.75:0.25:4.75 5:1:8]; 
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;Q_bins: position range from -40 to 40
Qdot_bins: velocity range from -6 to 6
Qdd_bins: acceleration range from -8 to 8&lt;/p&gt;
&lt;p&gt;Just change the limits of these three arrays and run the function directly, you will get the policy in .mat format.&lt;/p&gt;
&lt;ol start=&#34;3&#34;&gt;
&lt;li&gt;Transfer the mat to bin file policy.
Run saveThePolicy.m in the same folder, you will get the following bin files：&lt;/li&gt;
&lt;/ol&gt;
&lt;pre&gt;&lt;code&gt;load pi_hor; 
saveAsBin(dt,strcat(folderName,&#39;/&#39;,&#39;dt&#39;,&#39;.bin&#39;)); 
saveAsBin(PI,strcat(folderName,&#39;/&#39;,&#39;PI&#39;,&#39;.bin&#39;)); 
saveAsBin(q_bins,strcat(folderName,&#39;/&#39;,&#39;q_bins&#39;,&#39;.bin&#39;)); 
saveAsBin(qdd_bins,strcat(folderName,&#39;/&#39;,&#39;qdd_bins&#39;,&#39;.bin&#39;)); 
saveAsBin(qdot_bins,strcat(folderName,&#39;/&#39;,&#39;qdot_bins&#39;,&#39;.bin&#39;)); 
saveAsBin(J,strcat(folderName,&#39;/&#39;,&#39;J&#39;,&#39;.bin&#39;)); 
&lt;/code&gt;&lt;/pre&gt;
&lt;ol start=&#34;4&#34;&gt;
&lt;li&gt;Substitute these files under the /param/ctrlpred/pos_1/hor/ctrl folder in your project to apply the new policy.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;Note:  If you want to change the vertical position control policy, horizontal velocity control policy,  vertical velocity control policy, please modify the same params in corresponding files, e.g. trippleIntegrator_ver.m, double_Integrator_hor.m, double_Integrator_ver.m.&lt;/p&gt;
&lt;p&gt;To change the policy in MPC for target selection in path planning, you will need to retrain the network with the new policy. There is no ready to use scripts yet. I will have to write one for it.&lt;/p&gt;
&lt;h2 id=&#34;changes-made-for-target-tracking&#34;&gt;Changes made for target tracking&lt;/h2&gt;
&lt;ol&gt;
&lt;li&gt;
&lt;p&gt;reset the limits in trippleIntegrator_hor.m
q_bins = [-40:2:-12 -10:1:-5 -4.75:0.25:4.75 5:1:10 12:2:40];
qdot_bins = [-6:1:-5 -4.75:0.25:4.75 5:1:6];
qdd_bins = [-8:1:-5 -4.75:0.25:4.75 5:1:8];&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;add punishment in dynamic models due to the resistant force caused by the high speed&lt;/p&gt;
&lt;/li&gt;
&lt;/ol&gt;
&lt;pre&gt;&lt;code&gt;for i=1:n 
x_next(:,i)=A*x(:,i)+B*u(i); 
coef = 0.2 * x(:,i).^2 .* sign(x(:,i)); 
if(i == 1) x_next(:, i) = x_next(:, i) - coef * dt^2/2; end 
if(i == 2) x_next(:, i) = x_next(:, i) - coef * dt; end 
end 
&lt;/code&gt;&lt;/pre&gt;
&lt;ol start=&#34;3&#34;&gt;
&lt;li&gt;
&lt;p&gt;Lower down the punishment on v
Change Q from Q = diag([5 8 0.01]).*dt; to Q = diag([5 4 0.01]).*dt;&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Put punishment on a only when v&amp;gt;6 instead of doing it from all the time&lt;/p&gt;
&lt;/li&gt;
&lt;/ol&gt;
&lt;pre&gt;&lt;code&gt;if abs(Xerr(2,i))&amp;gt;6 
C(i)=C(i)+(abs(Xerr(2,i))-6)^2*20; 
if abs(Xerr(3,i))&amp;gt;3 
C(i)=C(i)+(abs(Xerr(3,i))-3)^2*30; 
end 
end 
&lt;/code&gt;&lt;/pre&gt;
&lt;h2 id=&#34;retrain-policy&#34;&gt;Retrain Policy&lt;/h2&gt;
&lt;p&gt;Vertical and horizontal are trained separately, so here we use the vertical in tripple_integrator as an example.&lt;/p&gt;
&lt;ol&gt;
&lt;li&gt;Generate new policy&lt;/li&gt;
&lt;/ol&gt;
&lt;ul&gt;
&lt;li&gt;Change the default folder to &amp;ldquo;nndp/DP_part/triple_integrator&amp;rdquo; and then run the command:&lt;/li&gt;
&lt;/ul&gt;
&lt;pre&gt;&lt;code&gt;$ trippleIntegrator_ver(true) 
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;Wait for about 1 minute till the error is below 0.01 or the iteration is larger than 1500. The following graph will be showing as the evaluation result of the generated policy: from top to down are jerk input, p, v, a. The initial value for is [p, v, a] = x = [-0.5 0 0]. The pi_ver.mat will be generated which contains: q_bins_ver qdot_bins_ver qdd_bins_ver PI_ver J_ver dt;&lt;/p&gt;
&lt;body&gt;&lt;img src=&#34;img/u_traj_ver.png&#34;  width=&#34;500&#34; height=&#34;300&#34; &gt;&lt;p style=&#34;text-align:center;&#34;&gt;control input u in vertical&lt;/p&gt;&lt;/body&gt;
&lt;body&gt;&lt;img src=&#34;img/policy_ver.png&#34;  width=&#34;500&#34; height=&#34;300&#34; &gt;&lt;p style=&#34;text-align:center;&#34;&gt; vertical policy&lt;/p&gt;&lt;/body&gt;
&lt;p&gt;Run $trippleIntegrator_hor(true) as well to get horizontal policy pi_hor.mat will be generated which contains: pi_hor.mat q_bins qdot_bins qdd_bins PI J dt. The initial state [p, v, a] = [-9, 0, 0] in this example.&lt;/p&gt;
&lt;body&gt;&lt;img src=&#34;img/u_traj_hor.png&#34;  width=&#34;500&#34; height=&#34;300&#34; &gt;&lt;p style=&#34;text-align:center;&#34;&gt;control input u in horizontal&lt;/p&gt;&lt;/body&gt;
&lt;body&gt;&lt;img src=&#34;img/policy_hor.png&#34;  width=&#34;500&#34; height=&#34;300&#34; &gt;&lt;p style=&#34;text-align:center;&#34;&gt; horizontal policy&lt;/p&gt;&lt;/body&gt;
&lt;ol start=&#34;2&#34;&gt;
&lt;li&gt;
&lt;p&gt;Save the generted policy in bin files (optional)
Run $saveThePolicy then you will get the ctrl* folder with all the bin files including ver and hor policy subfolders.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Generate training data (Takes two hours for 200000 iterations. Depends on the size of the training data you wanted to create)&lt;/p&gt;
&lt;/li&gt;
&lt;/ol&gt;
&lt;ul&gt;
&lt;li&gt;Change current folder to &amp;ldquo;nndp/Learning_part/tripple_integrator&amp;rdquo; and find DataGenertion*.m file.&lt;/li&gt;
&lt;li&gt;Change the policy folder path as &amp;ldquo;load ../../DP_part/triple_integrator/pi_ver.mat&amp;rdquo;&lt;/li&gt;
&lt;li&gt;Run $Data_Genertion_ver_D  for example to obtain the vertical training data. Similar steps for horizontal policy training data. Then the training_data_ver_small.mat or training_data_hor.mat
will be generated.&lt;/li&gt;
&lt;li&gt;Get the right format to feed in the neural network.&lt;/li&gt;
&lt;li&gt;Run $loaddata.m to transfer data from training_data_ver_small.mat into INISTATE.mat and TARGET.mat, or from  training_data_hor.mat into same format.&lt;/li&gt;
&lt;/ul&gt;
&lt;ol start=&#34;4&#34;&gt;
&lt;li&gt;Train the newly generated policy&lt;/li&gt;
&lt;/ol&gt;
&lt;ul&gt;
&lt;li&gt;Run testPython.ipynb to train the network with generated data. Make sure the data path and key is valid.&lt;/li&gt;
&lt;li&gt;Obtain array_0 to array_7.npy network parameters after training.&lt;/li&gt;
&lt;/ul&gt;
&lt;ol start=&#34;5&#34;&gt;
&lt;li&gt;Evaluated the newly trained policy&lt;/li&gt;
&lt;/ol&gt;
&lt;ul&gt;
&lt;li&gt;Run testNN in the array folder to get NN_*.mat&lt;/li&gt;
&lt;li&gt;Run compare to get the following figure, from which we can see the NN planed the trajectory is perfectly mimic the data generated from the policy.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;img src=&#34;img/testnn.png&#34; alt=&#34;&#34;&gt;&lt;/p&gt;
</description>
    </item>
    
  </channel>
</rss>
